from .utils import get_json


def remove_shits(data):
    print(data)
    _data = {
        "title": data["title"],
        "date": data["source"].get("createdAt"),
        # "ago": timeago.format(data["source"]["createdAt"]),
        "url": data["url"].get("target"),
        "author": data["source"].get("authorName", None),
        "comment_url": None,
        "image": data["image"].get("big", None)
    }
    return _data


def resource(source):
    sort = []
    if source["popular"]:
        sort.append("popular")
    if source["latest"]:
        sort.append("latest")

    subsources = [{"name": s["displayName"], "key":s["name"],
                   "source":s["source"]} for s in source["subsources"]]
    _data = {
        "name": source["name"],
        "key": source["key"],
        "icon": source["icon"],
        "subsources": subsources,
        "sorting": sort,
        "description": source.get("description", "")
    }
    return _data


def get_sources():
    url = "https://api.usepanda.com/v1.1/sources?integrations=true&languages=*"
    response = get_json(url)
    response = list(map(resource, response))
    return response


def get_feeds(source, **kwargs):

    limit = kwargs.get("limit", None) or 30
    sort = kwargs.get("sort", None) or "popular"
    page = kwargs.get("page", None) or 1
    subsource = kwargs.get("subsource", None)

    _subsource = ""
    if subsource:
        _subsource = f"&subsource={subsource}${source}"
        sort = "latest"

    url = f"https://api.usepanda.com/v2/feeds?limit={limit}&page={page}&sort={sort}&sources={source}{_subsource}"
    print(url)
    response = get_json(url)
    response = list(map(remove_shits, response))
    return response


def get_direct_feeds(source, sort):
    url = f"http://api.pnd.gs/v1/sources/{source}/{sort}"
    response = get_json(url)
    response = list(map(remove_shits, response))
    return response


def get_feed(source, url):
    url = f"https://api.usepanda.com/v2/feeds/story/full?feed={source}&url={url}"
    response = get_json(url)
    return response
