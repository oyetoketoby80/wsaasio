from bs4 import BeautifulSoup
from .utils import get_html, post_json

DEV_URLS = 'https://devurls.com/'
TECH_URLS = 'https://techurls.com/'
FIN_URLS = 'https://finurls.com/'
ALL_URLS = [DEV_URLS, TECH_URLS, FIN_URLS]


def scrape_link(html):
    link = html.select_one(".link")
    a = link.select_one("a")
    result = {
        'text': a.text,
        'url': a.get("href")
    }
    return result


def scrape_all_links(url):
    html = get_html(url)
    soup = BeautifulSoup(html, 'lxml')
    links = soup.select(".sites-container .publisher-block .publisher-link")

    results = map(scrape_link, links)
    results = list(results)

    return results


def scrape_all_urls():
    results = list(map(scrape_all_links, ALL_URLS))
    results = {
        "devurls": results[0],
        "techurls": results[1],
        "finurls": results[2]

    }
    return results


def scrape_publisher(html):
    title = html.select_one(".publisher-header .publisher-text .primary")
    url = html.select_one(".publisher-header .publisher-icon a")
    _links = html.select(".publisher-data .publisher-link")
    links = list(map(scrape_link, _links))
    data = {
        "title": title.text,
        "key": "".join(title.text.lower().split(" ")),
        "links": links,
        "url": url.get("href")
    }
    return data


def scrape_links_publisher(url):
    html = get_html(url)
    soup = BeautifulSoup(html, 'lxml')
    publishers = soup.select(".publisher-block .publisher-card")
    publishers_data = list(map(scrape_publisher, publishers))
    return publishers_data


def scrape_all_publisher_links():
    results = list(map(scrape_links_publisher, ALL_URLS))
    results = [ir for r in results for ir in r]
    # results = {
    #     "devurls": results[0],
    #     "techurls": results[1],
    #     "finurls": results[2]

    # }
    return results


def scrape_publisher_api_links(source, interval="latest", url=DEV_URLS):
    url = f"{url}/api/get_titles"
    headers = {
        'Origin': url,
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9,ha;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        'Content-type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Referer': url,
        'Connection': 'keep-alive',
    }
    body = {
        "site": source,
        "interval": interval
    }
    results = post_json(url, headers=headers, body=body)
    results = results["data"]
    return results


def get_all_publishers_api_links(**kwargs):
    source = kwargs.get("source", "medium")
    interval = kwargs.get("interval", "latest")

    _results = [scrape_publisher_api_links(
        source, interval, url) for url in ALL_URLS]
    results = [_r for r in _results for _r in r]
    return results


def get_random_api_medium_links(interval="latest"):
    source = "medium"
    devurls = scrape_publisher_api_links(source, interval=interval)
    techurls = scrape_publisher_api_links(
        source, interval=interval, url=TECH_URLS)
    finurls = scrape_publisher_api_links(
        source, interval=interval, url=FIN_URLS)
    return devurls + techurls + finurls


def search_url_links(url, q):
    _url = f"{url}/api/search"
    headers = {
        'Origin': url,
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9,ha;q=0.8',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
        'Content-type': 'application/x-www-form-urlencoded',
        'Accept': '*/*',
        'Referer': url,
        'Connection': 'keep-alive',
    }
    body = {"q": q}
    data = post_json(_url, headers=headers, body=body)
    results = data["data"]
    return results


def search_links(q):
    _results = [search_url_links(url, q) for url in ALL_URLS]
    results = [_r for r in _results for _r in r]
    return results
