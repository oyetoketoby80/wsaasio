from .utils import post_json
import json

headers = {
    'Accept': 'application/json',
    'Referer': 'https://scrapinghub.com/autoextract',
    'Origin': 'https://scrapinghub.com',
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36',
    'Content-Type': 'application/json',
}


def extract(url, page_type):
    data = {"url": url, "page_type": page_type}
    _url = "https://xod-beta-forwarder.scrapinghub.com/extract"
    response = post_json(_url, headers=headers, body=json.dumps(data))
    return response
