from .utils import get_html
from bs4 import BeautifulSoup

URL = "https://ww.ffmovies.cc"


def scrape_movies(html):
    name = html.select_one("a.name")
    url = name.get("href")
    image = html.select_one("a.poster img")
    quality = html.select_one(".quality")
    episode = html.select_one(".status span")
    data = {
        "name": name.text,
        "poster": image.get("src"),
        "url": url
    }
    if quality:
        data.update({"quality": quality.text, "is_series": False})
    if episode:
        data.update({"episode": episode.text, "is_series": True})

    return data


def get_next_page(html):
    current_page = html.select_one(".pagination li.active")
    if current_page:
        next_page = current_page.nextSibling
        next_page = next_page.select_one("a")
        return next_page.text
    return None


def search_movies(query, page=None):
    query = "+".join(query.split(" "))
    url = f"{URL}/search-query/{query}"
    if page:
        url += f"/page/{page}"
    html = get_html(url)
    soup = BeautifulSoup(html, "lxml")
    next_page = get_next_page(soup)
    movies = soup.select(".movie-list .col-lg-3 .item")

    results = map(scrape_movies, movies)
    data = {
        "movies": list(results),
        "next_page": next_page
    }
    return data


def get_movies(**kwargs):
    category = kwargs.get("category", "fmovies")
    genre = kwargs.get("genre", None)
    country = kwargs.get("country", None)
    release_year = kwargs.get("release_year", None)
    page = kwargs.get("page", None)

    if genre:
        url = f"{URL}/genre/{genre}"
    elif country:
        url = f"{URL}/country/{country}"
    elif release_year:
        url = f"{URL}/release-year/{release_year}"
    else:
        url = f"{URL}/{category}"

    if category == "fmovies" and genre == None and country == None and release_year == None:
        page = None

    if category == "most_watched":
        url = f"{URL}/movie/filter/all/view/all/all/all/all"

    if page:
        url += f"/page/{page}"

    html = get_html(url)

    soup = BeautifulSoup(html, "lxml")
    movies = soup.select(".movie-list .col-lg-3 .item")
    results = map(scrape_movies, movies)
    data = {
        "movies": list(results),
        "next_page": get_next_page(soup)
    }
    return data


def get_movie_info(url):
    html = get_html(url)
    soup = BeautifulSoup(html, "lxml")

    cover = soup.select_one("#player .cover").get(
        "style").replace("background-image: url('", "")
    info = soup.select_one("#info .row")

    image = info.select_one(".thumb img")

    title = info.select_one(".info .name")
    description = info.select_one(".info .desc .fullcontent")
    meta = info.select(".info .meta span b")
    imdb = meta[0]
    minutes = meta[1]
    other_details = info.select(".info .row dl dd")
    genre = other_details[0]
    actors = other_details[1]
    director = other_details[2]
    release_date = other_details[3]
    quality = other_details[4]
    country = other_details[5]
    data = {
        "title": title.text,
        "image": image.get("src"),
        "cover_image": cover,
        "url": url,
        "description": description.text.strip(),
        "imdb": imdb.text,
        "minutes": minutes.text,
        "genre": genre.text,
        "actors": actors.text,
        "director": director.text,
        "release_date": release_date.text,
        "quality": quality.text,
        "country": country.text
    }
    return data
