from .utils import get_json

URL = "https://jobreed.herokuapp.com/api/v1/search"

API_KEY = "ApKWqH589d3d4825dc24f65f47a9471e5e0ab2HQAeu"


def search_jobs(**kwargs):
    queries = "&".join([f"{q}={k[0]}" for q, k in kwargs.items()])
    url = f"{URL}?{queries}&api_key={API_KEY}"
    data = get_json(url)
    return data
