from bs4 import BeautifulSoup
from .utils import get_html

URL = "https://www.pdfdrive.com"


def get_result_number(html):
    result_found = html.select_one("#result-found strong")
    if result_found:
        return result_found.text
    return None


def get_number_of_pages(html):
    pagination = html.select(".pagination .Zebra_Pagination li a")
    last_pagination = pagination[-2].text
    return last_pagination


def get_next_page(html):
    current = html.select_one(".pagination .Zebra_Pagination li a.current")
    parent = current.parent
    next_pagination = parent.nextSibling
    next_pagination = next_pagination.select_one("a").text

    return next_pagination


def scrape_files(file):
    file_right = file.select_one(".file-right")
    if file_right:
        ut = file_right.select_one("a")
        file_url = ut.get("href")
        title = ut.text
        file_info = file_right.select_one(".file-info")
        page_count = file_info.select_one("span.fi-pagecount")
        year = file_info.select_one("span.fi-year")
        size = file_info.select_one("span.fi-size")
        downloads = file_info.select_one("span.fi-hit")

        file_left = file.select_one(".file-left")
        image = file_left.select_one("a img.file-img")

        file_dict = {
            "title": title.strip("\n").strip("\\n"),
            "url": URL+file_url,
            "image": image.get("src"),
            "pages": page_count.text,
            "year": year.text,
            "size": size.text,
            "downloads": downloads.text
        }
        return file_dict


def search_pdfs(query, **kwargs):
    page_count = kwargs.get("page_count", "")
    pub_year = kwargs.get("pub_year", "")
    search_in = kwargs.get("search_in", "")
    page = kwargs.get("page", None)

    if " " in query:
        query = "+".join(query.split(" "))
    url = f"{URL}/search?q={query}&pagecount={page_count}&pubyear={pub_year}&searchin={search_in}"
    if page:
        url += f"&page={page[0]}"

    print(url, page)
    html = str(get_html(url))
    soup = BeautifulSoup(html, "lxml")
    # result_number = get_result_number(soup)
    number_of_pages = get_number_of_pages(soup)
    next_page = get_next_page(soup)

    files = soup.select("div.files-new ul li")

    files_result = list(map(scrape_files, files))
    data = {
        "files": files_result,
        # "result-found": result_number,
        "pages": number_of_pages,
        "next_page": next_page
    }
    return data


def get_pdfs():
    html = get_html(URL)
    soup = BeautifulSoup(html, "lxml")

    files = soup.select("div.files-new ul li")

    files_result = list(map(scrape_files, files))
    return files_result
