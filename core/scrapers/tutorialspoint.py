from .utils import get_html
from bs4 import BeautifulSoup

URL = "https://www.tutorialspoint.com"


def get_tutorial_image(url):
    try:
        image_namespace = url.split("/")[1]
    except:
        image_namespace = url.split("/")[0].replace(".htm", "")
    mini_image = "-".join(image_namespace.split("_"))
    image = f"{URL}/{image_namespace}/images/{mini_image}-mini-logo.jpg"
    return image


def extract_tutorials(html):
    name = html.select_one("a")
    _url = name.get('href')
    url = f"{URL}{_url}"
    if _url[0] != "/":
        url = f"{URL}/{_url}"
    image = get_tutorial_image(_url)
    return {
        "name": name.text,
        "url": url,
        "image": image
    }


def get_categories(html):
    h4s = html.select("h4")
    uls = html.select("ul.menu")
    tuts = [{h.text: list(map(extract_tutorials, u.select("li")))}
            for (h, u) in zip(h4s, uls)]
    return tuts


def scrape_tutorials(html):

    featured_box = html.select(".mui-container .featured-box")
    categories = list(map(get_categories, featured_box))
    tutorials = [_k for cat in categories for _cat in cat for c, k in _cat.items()
                 for _k in k]
    return tutorials


def get_all_tutorials():
    url = f"{URL}/tutorialslibrary.htm"
    html = get_html(url)
    soup = BeautifulSoup(html, "lxml")
    tutorials = scrape_tutorials(soup)
    return tutorials
