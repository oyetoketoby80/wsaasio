import requests
import cfscrape
cfscrape.DEFAULT_CIPHERS += ':!SHA'


def get_html(url, headers={}):
    request = requests.get(url, headers=headers)
    print(request.status_code, url)
    if request.status_code == 200:
        return request.content
    else:
        raise Exception("Bad request")


def post_json(url, headers={}, body={}):
    request = requests.post(url, headers=headers, data=body)
    if request.status_code == 200:
        # print(request.json())
        return request.json()
    else:
        raise Exception("Bad request")


def get_json(url, headers={}):
    request = requests.get(url, headers=headers)
    print(request.status_code, url)
    if request.status_code == 200:
        return request.json()
    else:
        raise Exception("Bad request")


def get_cloudfare_html(url):
    scraper = cfscrape.create_scraper()
    request = scraper.get(url).content
    return request
