from django.urls import path, include
from core.views import links, quick_links, search_links, feeds, feed_sources, books, books_search, tutorials, jobs_view, extract, movies, movie_info, search_movies
urlpatterns = [
    path('quick_links', quick_links, name="quick_links"),
    path('links', links, name="links"),
    path('links/search', search_links, name="search_links"),
    path('feeds', feeds, name="feeds"),
    path('feeds/sources', feed_sources, name="feed_sources"),
    path('books', books, name="books"),
    path('books/search', books_search, name="books_search"),
    path('tutorials', tutorials, name="tutorials"),
    path('jobs', jobs_view, name="jobs"),
    path('extract', extract, name="extract"),
    path('movies', movies, name="movies"),
    path('movies/search', search_movies, name="search_movies"),
    path('movies/info', movie_info, name="movie_info"),
]
