from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from core.scrapers import allurls, allarticles, tutorialspoint, pdfdrive, jobs, extractor, ffmovies


@api_view(['GET'])
@permission_classes((AllowAny,))
def quick_links(request):
    data = allurls.scrape_all_publisher_links()
    return Response(data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def links(request):
    query = request.GET.copy()
    data = allurls.get_all_publishers_api_links(**query)
    return Response(data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def search_links(request):
    query = request.GET.copy()
    q = query.get("q", None)
    if not q:
        return Response(
            status=400, data={"error": "Search query {q} must be given"})
    data = allurls.search_links(q)
    return Response(data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def feeds(request):
    query = request.GET.copy()

    if not query:
        return Response(
            status=400, data={"error": "Queries cannot be empty"})

    source = query.pop("source", None)[0]
    avalaible_queries = ["limit", "sort", "page", "subsource"]
    queries = {qk: qv for qk, qv in query.items() if qk in avalaible_queries}
    # try:
    posts = allarticles.get_feeds(source, **queries)
    # except:
    #     return Response(
    #         status=500, data={"error": "Unknown error occured"})

    return Response(data=posts, status=200)


@api_view(['GET'])
@permission_classes((AllowAny,))
def feed_sources(request):

    data = allarticles.get_sources()
    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def books(request):
    query = request.GET.copy()

    data = pdfdrive.get_pdfs()
    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def books_search(request):
    query = request.GET.copy()
    if not query:
        return Response(
            status=400, data={"error": "Queries cannot be empty"})
    q = query.get("q")

    data = pdfdrive.search_pdfs(q, **query)

    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def tutorials(request):
    data = tutorialspoint.get_all_tutorials()

    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def jobs_view(request):
    queries = request.GET.copy()

    data = jobs.search_jobs(**queries)

    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def extract(request):
    queries = request.GET.copy()
    url = queries.get("url", None)
    page_type = queries.get("type", "article")
    if not url:
        return Response(
            status=400, data={"error": "URL Param is not given. Provide an article or product url to be scraped"})

    data = extractor.extract(url, page_type)

    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def movies(request):
    queries = request.GET.copy()

    data = ffmovies.get_movies(**queries)

    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def search_movies(request):
    queries = request.GET.copy()
    q = queries.get("q", None)
    p = queries.get("page", None)
    if not q:
        return Response(
            status=400, data={"error": "Search query {q} must be given"})

    data = ffmovies.search_movies(q, p)

    return Response(data=data)


@api_view(['GET'])
@permission_classes((AllowAny,))
def movie_info(request):
    queries = request.GET.copy()
    url = queries.get("url", None)
    if not url:
        return Response(
            status=400, data={"error": "URL Param is not given. Provide an ffmovies url to be scraped"})

    data = ffmovies.get_movie_info(url)
    print(data)

    return Response(data=data)
