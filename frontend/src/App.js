import React, { useState } from "react";
import "./App.css";
import Categories from "./Components/Categories";
import Explorer from "./Components/Explorer";

function App() {
  const [tabActive, setTab] = useState("links");

  return (
    <div className="App">
      <Categories tabActive={tabActive} setTab={setTab} />
      <Explorer tabActive={tabActive} />
    </div>
  );
}

export default App;
