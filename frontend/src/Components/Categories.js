import React, { useState, useEffect } from "react";
import { Tab, Tabs, TabList, TabLink, Icon } from "bloomer";

const tabs = [
  { name: "Quick Links", key: "links", icon: "fa fa-link" },
  { name: "Articles", key: "articles", icon: "fa fa-link" },
  { name: "Books", key: "books", icon: "fa fa-link" },
  { name: "Tutorials", key: "tutorials", icon: "fa fa-link" },
  { name: "Jobs", key: "jobs", icon: "fa fa-link" },
  { name: "Movies", key: "movies", icon: "fa fa-link" },
  { name: "Auto Data Extractor", key: "extractor", icon: "fa fa-link" }
];
export default ({ tabActive, setTab }) => {
  return (
    <>
      <Tabs>
        <TabList>
          {tabs.map((tab, key) => {
            return (
              <Tab
                key={tab.key}
                onClick={() => {
                  setTab(tab.key);
                }}
                isActive={tabActive === tab.key ? true : false}
              >
                <TabLink>
                  <Icon isSize="small">
                    <span className={tab.icon} aria-hidden="true" />
                  </Icon>
                  <span>{tab.name}</span>
                </TabLink>
              </Tab>
            );
          })}
        </TabList>
      </Tabs>
    </>
  );
};
