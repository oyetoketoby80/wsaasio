import React, { useState, useEffect } from "react";
import {
  Container,
  Control,
  Input,
  Icon,
  Panel,
  PanelBlock,
  PanelHeading,
  PanelTab,
  PanelTabs,
  PanelIcon,
  Checkbox,
  Button,
  Column,
  Columns
} from "bloomer";
import { useFetch } from "../../../hooks";
import DataView from "../../DataView";
import Loader from "../../Loader";
import axios from "axios";

export default ({ apiUrl }) => {
  const sources_url = `${apiUrl}/feeds/sources`;
  //   const [sources, fetchSources, reload, setSourcesResponse] = useFetch(
  //     sources_url
  //   );
  const [source, setSource] = useState("behance");

  const [sources, setSources] = useState([]);
  const [oldSource, setOldSources] = useState([]);

  const [results, fetchData, reloadResults] = useFetch(
    `${apiUrl}/feeds?source=${source}`
  );
  useEffect(() => {
    axios.get(sources_url).then(res => {
      setSources(res.data);
      setOldSources(res.data);
    });
  }, []);
  const search = e => {
    const searchTerm = e.target.value;
    let filteredResults = [];

    if (searchTerm) {
      oldSource.map(obj => {
        if (obj.name.toLowerCase().includes(searchTerm.toLowerCase())) {
          filteredResults.push(obj);
        }
      });
    } else {
      filteredResults = oldSource;
    }
    setSources(filteredResults);
  };
  return (
    <Container>
      <Columns>
        <Column isSize={3}>
          <Panel>
            <PanelHeading>Sources</PanelHeading>
            <PanelBlock>
              <Control hasIcons="left">
                <Input
                  isSize="small"
                  placeholder="Search"
                  onChange={e => {
                    search(e);
                  }}
                />
                <Icon isSize="small" isAlign="left">
                  <span className="fa fa-search" aria-hidden="true" />
                </Icon>
              </Control>
            </PanelBlock>
            {/* <PanelTabs>
              <PanelTab isActive>All</PanelTab>
              <PanelTab>Public</PanelTab>
              <PanelTab>Private</PanelTab>
              <PanelTab>Sources</PanelTab>
              <PanelTab>Fork</PanelTab>
            </PanelTabs> */}
            {sources.length == 0 && (
              <PanelBlock>
                <Loader />
              </PanelBlock>
            )}

            {sources && (
              <>
                {sources.map((s, k) => {
                  return (
                    <PanelBlock
                      key={s.key}
                      isActive={source === s.key ? true : false}
                      onClick={() => {
                        setSource(s.key);
                        fetchData(`${apiUrl}/feeds?source=${s.key}`);
                      }}
                    >
                      <PanelIcon
                        style={{
                          backgroundImage: `url(${s.icon})`,
                          backgroundSize: "cover"
                        }}
                      />
                      {s.name}
                    </PanelBlock>
                  );
                })}
              </>
            )}
          </Panel>
        </Column>
        <Column isSize={9}>
          {results.loading && <Loader />}
          {!results.loading && <DataView data={results.data} />}
        </Column>
      </Columns>
    </Container>
  );
};
