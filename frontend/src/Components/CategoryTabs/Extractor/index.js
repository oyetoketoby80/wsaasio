import React, { useState } from "react";
import {
  Container,
  Control,
  Input,
  Icon,
  Button,
  Column,
  Columns,
  Select
} from "bloomer";
import { useFetch } from "../../../hooks";
import DataView from "../../DataView";
import Loader from "../../Loader";

export default ({ apiUrl }) => {
  const [response, fetchData, reload] = useFetch();
  const [pageUrl, setPageUrl] = useState("");
  const [pageType, setPageType] = useState("article");

  return (
    <Container>
      <Columns>
        <Column isSize={8}>
          <Control hasIcons="left" style={{ marginBottom: "10px" }}>
            <Input
              placeholder="Url"
              onChange={e => {
                setPageUrl(e.target.value);
              }}
              type="url"
            />
            <Icon isSize="small" isAlign="left">
              <span className="fa fa-search" aria-hidden="true" />
            </Icon>
          </Control>
        </Column>
        <Column isSize={3}>
          <Control style={{ marginBottom: "10px" }}>
            <Select
              onChange={e => {
                setPageType(e.target.value);
              }}
              value={pageType}
            >
              <option value="article">Article</option>
              <option value="product">Product</option>
            </Select>
          </Control>
        </Column>
        <Column isSize={1}>
          <Button
            isColor="primary"
            onClick={() => {
              fetchData(`${apiUrl}/extract?url=${pageUrl}&type=${pageType}`);
            }}
          >
            Extract
          </Button>
        </Column>
      </Columns>

      {response.loading && <Loader />}

      {!response.loading && <DataView data={response.data} />}
    </Container>
  );
};
