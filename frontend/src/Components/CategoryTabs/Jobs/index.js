import React, { useState } from "react";
import {
  Container,
  Control,
  Input,
  Icon,
  Button,
  Column,
  Columns,
  Select
} from "bloomer";
import { useFetch } from "../../../hooks";
import DataView from "../../DataView";
import Loader from "../../Loader";

const countries = Object.entries({
  us: { url: "http://www.indeed.com", fullname: "United States" },
  ch: { url: "http://www.indeed.ch", fullname: "Switzerland" },
  co: { url: "http://co.indeed.com", fullname: "Colombia" },
  cn: { url: "http://cn.indeed.com", fullname: "China" },
  ae: { url: "http://www.indeed.ae", fullname: "United Arab Emirates" },
  cl: { url: "http://www.indeed.cl", fullname: "Chile" },
  ve: { url: "http://ve.indeed.com", fullname: "Venezuela" },
  ca: { url: "http://ca.indeed.com", fullname: "Canada" },
  it: { url: "http://it.indeed.com", fullname: "Italy" },
  ec: { url: "http://ec.indeed.com", fullname: "Ecuador" },
  cz: { url: "http://cz.indeed.com", fullname: "Czech Republic" },
  ar: { url: "http://ar.indeed.com", fullname: "Argentina" },
  au: { url: "http://au.indeed.com", fullname: "Australia" },
  at: { url: "http://at.indeed.com", fullname: "Austria" },
  in: { url: "http://www.indeed.co.in", fullname: "India" },
  cr: { url: "http://cr.indeed.com", fullname: "Costa Rica" },
  ie: { url: "http://ie.indeed.com", fullname: "Ireland" },
  id: { url: "http://id.indeed.com", fullname: "Indonesia" },
  es: { url: "http://www.indeed.es", fullname: "Spain" },
  gr: { url: "http://gr.indeed.com", fullname: "Greece" },
  ru: { url: "http://ru.indeed.com", fullname: "Russia" },
  nl: { url: "http://www.indeed.nl", fullname: "Netherlands" },
  pt: { url: "http://www.indeed.pt", fullname: "Portugal" },
  no: { url: "http://no.indeed.com", fullname: "Norway" },
  pa: { url: "http://pa.indeed.com", fullname: "Panama" },
  tr: { url: "http://tr.indeed.com", fullname: "Turkey" },
  ng: { url: "http://ng.indeed.com", fullname: "Nigeria" },
  nz: { url: "http://nz.indeed.com", fullname: "New Zealand" },
  lu: { url: "http://www.indeed.lu", fullname: "Luxembourg" },
  th: { url: "http://th.indeed.com", fullname: "Thailand" },
  pe: { url: "http://www.indeed.com.pe", fullname: "Peru" },
  pk: { url: "http://www.indeed.com.pk", fullname: "Pakistan" },
  ph: { url: "http://www.indeed.com.ph", fullname: "Philippines" },
  ro: { url: "http://ro.indeed.com", fullname: "Romania" },
  eg: { url: "http://eg.indeed.com", fullname: "Egypt" },
  pl: { url: "http://pl.indeed.com", fullname: "Poland" },
  be: { url: "http://be.indeed.com", fullname: "Belgium" },
  fr: { url: "http://www.indeed.fr", fullname: "France" },
  dk: { url: "http://dk.indeed.com", fullname: "Denmark" },
  jp: { url: "http://jp.indeed.com", fullname: "Japan" },
  de: { url: "http://de.indeed.com", fullname: "Germany" },
  bh: { url: "http://bh.indeed.com", fullname: "Bahrain" },
  hu: { url: "http://hu.indeed.com", fullname: "Hungary" },
  za: { url: "http://www.indeed.co.za", fullname: "South Africa" },
  hk: { url: "http://www.indeed.hk", fullname: "Hong Kong" },
  vn: { url: "http://vn.indeed.com", fullname: "Vietnam" },
  br: { url: "http://www.indeed.com.br", fullname: "Brazil" },
  fi: { url: "http://www.indeed.fi", fullname: "Finland" },
  om: { url: "http://om.indeed.com", fullname: "Oman" },
  ma: { url: "http://ma.indeed.com", fullname: "Morocco" },
  sg: { url: "http://www.indeed.com.sg", fullname: "Singapore" },
  qa: { url: "http://qa.indeed.com", fullname: "Qatar" },
  kr: { url: "http://kr.indeed.com", fullname: "South Korea" },
  ua: { url: "http://ua.indeed.com", fullname: "Ukraine" },
  kw: { url: "http://kw.indeed.com", fullname: "Kuwait" },
  uk: { url: "http://www.indeed.co.uk", fullname: "United Kingdom" },
  tw: { url: "http://tw.indeed.com", fullname: "Taiwan" },
  uy: { url: "http://uy.indeed.com", fullname: "Uruguay" },
  sa: { url: "http://sa.indeed.com", fullname: "Saudi Arabia" },
  my: { url: "http://www.indeed.com.my", fullname: "Malaysia" },
  mx: { url: "http://www.indeed.com.mx", fullname: "Mexico" },
  se: { url: "http://se.indeed.com", fullname: "Sweden" },
  il: { url: "http://il.indeed.com", fullname: "Israel" }
});

export default ({ apiUrl }) => {
  const [query, setQuery] = useState("python");
  const [l, setL] = useState("lagos");
  const [country, setCountry] = useState("ng");
  const url = `${apiUrl}/jobs?q=${query}&l=${l}&country=${country}`;
  const [response, fetchData, reload] = useFetch(url);

  return (
    <Container>
      <Columns>
        <Column isSize={6}>
          <Control hasIcons="left" style={{ marginBottom: "10px" }}>
            <Input
              placeholder="Job"
              onChange={e => {
                setQuery(e.target.value);
              }}
              value={query}
            />
            <Icon isSize="small" isAlign="left">
              <span className="fa fa-search" aria-hidden="true" />
            </Icon>
          </Control>
        </Column>
        <Column isSize={3}>
          <Control style={{ marginBottom: "10px" }}>
            <Input
              placeholder="Location"
              onChange={e => {
                setL(e.target.value);
              }}
              value={l}
            />
            <Icon isSize="small" isAlign="left">
              <span className="fa fa-search" aria-hidden="true" />
            </Icon>
          </Control>
        </Column>
        <Column isSize={2}>
          <Control style={{ marginBottom: "10px" }}>
            <Select
              onChange={e => {
                setCountry(e.target.value);
              }}
              value={country}
            >
              {countries.map((c, k) => {
                return (
                  <option value={c[0]} key={c[0]}>
                    {c[1].fullname}
                  </option>
                );
              })}
            </Select>
          </Control>
        </Column>
        <Column isSize={1}>
          <Button
            isColor="primary"
            onClick={() => {
              fetchData(`${apiUrl}/books/search?q=${query}&`);
            }}
          >
            Search
          </Button>
        </Column>
      </Columns>

      {response.loading && <Loader />}

      {!response.loading && <DataView data={response.data} />}
    </Container>
  );
};
