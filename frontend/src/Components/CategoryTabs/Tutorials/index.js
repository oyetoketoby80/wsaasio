import React, { useState } from "react";
import {
  Container,
  Control,
  Input,
  Icon,
  Button,
  Column,
  Columns
} from "bloomer";
import { useFetch } from "../../../hooks";
import DataView from "../../DataView";
import Loader from "../../Loader";

export default ({ apiUrl }) => {
  const url = `${apiUrl}/tutorials`;
  const [response, fetchData, reload, setResponse] = useFetch(url);
  const [query, setQuery] = useState("");
  const search = e => {
    const searchTerm = e;
    let filteredResults = [];

    if (searchTerm) {
      response.data.map(obj => {
        if (obj.name.toLowerCase().includes(searchTerm.toLowerCase())) {
          filteredResults.push(obj);
        }
      });
    } else {
      //   reload();
      filteredResults = response.data;
    }
    const sourceData = { loading: false, error: null, data: filteredResults };
    setResponse(sourceData);
  };
  return (
    <Container>
      <Control hasIcons="left" style={{ marginBottom: "10px" }}>
        <Columns>
          <Column isSize={11}>
            <Input
              placeholder="Search"
              onChange={e => {
                setQuery(e.target.value);
              }}
            />
            <Icon isSize="small" isAlign="left">
              <span className="fa fa-search" aria-hidden="true" />
            </Icon>
          </Column>
          <Column isSize={1}>
            <Button
              isColor="primary"
              onClick={() => {
                search(query);
              }}
            >
              Search
            </Button>
          </Column>
        </Columns>
      </Control>

      {response.loading && <Loader />}

      {!response.loading && <DataView data={response.data} />}
    </Container>
  );
};
