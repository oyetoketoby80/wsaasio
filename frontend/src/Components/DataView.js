import React from "react";
import ReactJson from "react-json-view";

import JSONPretty from "react-json-pretty";
export default ({ data, mode = "pretty" }) => {
  return (
    <>
      {mode == "pretty" && <JSONPretty id="json-pretty" data={data} />}

      {mode == "stylish" && (
        <ReactJson
          src={data}
          theme={"ocean"}
          displayObjectSize={false}
          displayDataTypes={false}
        />
      )}
    </>
  );
};
