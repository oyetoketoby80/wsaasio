import React from "react";
import { Container } from "bloomer";

import Links from "./CategoryTabs/Links";
import Articles from "./CategoryTabs/Articles";
import Books from "./CategoryTabs/Books";
import Tutorials from "./CategoryTabs/Tutorials";
import Jobs from "./CategoryTabs/Jobs";
import Movies from "./CategoryTabs/Movies";
import Extractor from "./CategoryTabs/Extractor";

import { apiURL } from "../config";

export default ({ tabActive }) => {
  const tabs = {
    links: <Links apiUrl={apiURL} />,
    articles: <Articles apiUrl={apiURL} />,
    books: <Books apiUrl={apiURL} />,
    tutorials: <Tutorials apiUrl={apiURL} />,
    jobs: <Jobs apiUrl={apiURL} />,
    movies: <Movies apiUrl={apiURL} />,
    extractor: <Extractor apiUrl={apiURL} />
  };
  return <>{tabs[tabActive]}</>;
};
