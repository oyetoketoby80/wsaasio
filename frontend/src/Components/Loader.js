import React from "react";

export default () => {
  return (
    <div style={{ textAlign: "center" }}>
      <div className="lds-roller">
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
};
