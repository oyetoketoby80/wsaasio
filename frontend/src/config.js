const baseApiUrlLocal = "http://localhost:7200/api/v1";
const baseApiUrlProd = "https://wsaasio.herokuapp.com/api/v1";

// API URL
const apiURL =
  process.env.NODE_ENV === "production" ? baseApiUrlProd : baseApiUrlLocal;

module.exports = {
  apiURL
};
