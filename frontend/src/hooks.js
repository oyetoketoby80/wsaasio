import { useEffect, useState } from "react";
import axios from "axios";

const dataInitialState = { loading: true, error: null, data: null };

export const useFetch = (url, queryParam = "", options) => {
  const [response, setResponse] = useState(dataInitialState);
  const [reloadRef, setReloadRef] = useState(Math.random());

  const fetchData = async (u = url, q = queryParam, o = options) => {
    if (url === null || u === null) {
      return;
    }
    setResponse({ loading: true });
    try {
      const res = await axios.get(`${u}${q}`, o);
      setResponse({
        data: res.data,
        loading: false,
        error: false
      });
    } catch (error) {
      setResponse({ loading: false, error });
    }
  };

  const reload = () => setReloadRef(Math.random());

  useEffect(() => {
    fetchData();
  }, [url, reloadRef]);
  return [response, fetchData, reload, setResponse];
};
