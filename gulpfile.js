const gulp = require("gulp");
const rename = require("gulp-rename");
const del = require("del");
var exec = require("child_process").exec;

function copyApp() {
  return gulp
    .src("./templates/app.html")
    .pipe(rename("index.html"))
    .pipe(gulp.dest("./frontend/public"));
}

function buildReact(cb) {
  exec("npm run build --prefix frontend", function(err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
}

function copyBuild() {
  return gulp
    .src("./frontend/build/index.html")
    .pipe(gulp.dest("./templates/app"));
}

function cleanJS() {
  return del(["./frontend/static/js"]);
}

function cleanCSS() {
  return del(["./frontend/static/css"]);
}

function copyCSS() {
  return gulp
    .src("./frontend/build/static/css/*.css")
    .pipe(gulp.dest("./frontend/static/css"));
}

function copyJS() {
  return gulp
    .src("./frontend/build/static/js/*.js")
    .pipe(gulp.dest("./frontend/static/js"));
}

function watch() {
  gulp.watch(
    "./templates/app.html",
    gulp.series(
      copyApp,
      buildReact,
      cleanJS,
      cleanCSS,
      gulp.parallel(copyBuild, copyJS, copyCSS)
    )
  );
  gulp.watch(
    "./frontend/src/**/*.js",
    gulp.series(
      buildReact,
      cleanJS,
      cleanCSS,
      gulp.parallel(copyBuild, copyJS, copyCSS)
    )
  );
}

exports.copyApp = copyApp;
exports.buildReact = buildReact;
exports.copyBuild = copyBuild;
exports.copyJS = copyJS;
exports.cleanJS = cleanJS;
exports.copyCSS = copyCSS;
exports.cleanCSS = cleanCSS;
exports.watch = watch;
exports.build = gulp.series(
  copyApp,
  buildReact,
  cleanJS,
  cleanCSS,
  copyBuild,
  copyJS,
  copyCSS
);
exports.default = gulp.series(
  copyApp,
  buildReact,
  cleanJS,
  cleanCSS,
  copyBuild,
  copyJS,
  copyCSS,
  watch
);
